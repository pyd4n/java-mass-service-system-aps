package polytech.system;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import polytech.statistics.BidLifecycleTimings;
import polytech.statistics.ProducerStatistics;
import polytech.statistics.StatisticsHolder;
import polytech.system.components.bid.Bid;
import polytech.system.components.bid.BidStatus;
import polytech.system.components.buffer.Buffer;
import polytech.system.components.buffer.BufferPlacementAndRejectionDispatcher;
import polytech.system.components.device.Device;
import polytech.system.components.discipline.BidSelectionDiscipline;
import polytech.system.components.discipline.DeviceSelectionDiscipline;
import polytech.system.components.event.Event;
import polytech.system.components.event.EventIdGenerator;
import polytech.system.components.producer.Producer;
import polytech.system.time.distribution.PoissonDistributionLaw;
import polytech.system.time.distribution.UniformDistributionLaw;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Контроллер для управления СМО.
 */
@Data
@RequiredArgsConstructor
public class MassServiceSystemController {
    /**
     * Источники заявок.
     */
    private final List<Producer> producers = new ArrayList<>();

    /**
     * Приборы.
     */
    private final List<Device> devices = new ArrayList<>();

    /**
     * Дисциплина постановки на обслуживание (Д2П1).
     */
    private final DeviceSelectionDiscipline deviceSelectionDiscipline;

    /**
     * Буфер.
     */
    private final Buffer buffer;

    /**
     * Диспетчер постановки заявок в буфер.
     */
    private final BufferPlacementAndRejectionDispatcher bufferPlacementAndRejectionDispatcher;

    /**
     * Дисциплина выбора заявки из буфера.
     */
    private final BidSelectionDiscipline bidSelectionDiscipline;

    /**
     * События, происходящие в СМО.
     */
    private final NavigableSet<Event> events = new TreeSet<>(Comparator
        .comparingDouble(Event::getTime)
        .thenComparing(Event::getId));

    /**
     * Генератор id'шников для event'ов.
     */
    private final EventIdGenerator eventIdGenerator = EventIdGenerator.getInstance();

    /**
     * Пуассоновский закон распределения.
     */
    private final PoissonDistributionLaw poissonDistributionLaw = new PoissonDistributionLaw();

    /**
     * Равномерный закон распределения.
     */
    private final UniformDistributionLaw uniformDistributionLaw = new UniformDistributionLaw();

    /**
     * Параметры СМО.
     */
    private final MassServiceSystemParams serviceSystemParams;

    /**
     * Статистика СМО.
     */
    private final StatisticsHolder statistics = StatisticsHolder.getInstance();

    /**
     * Создание системы массового обслуживания.
     *
     * @param serviceSystemParams - параметры СМО.
     */
    public MassServiceSystemController(MassServiceSystemParams serviceSystemParams) {
        initializeProducers(serviceSystemParams.getProducerCount());
        initializeDevices(serviceSystemParams.getDeviceCount());
        this.statistics.setProducerStatistics(producers.stream().collect(Collectors.toMap(Producer::getId, a -> new ProducerStatistics())));
        this.statistics.setDeviceWorkTime(devices.stream().collect(Collectors.toMap(Device::getId, a -> 0.0)));

        this.serviceSystemParams = serviceSystemParams;
        initializeFirstBids();
        this.buffer = new Buffer(serviceSystemParams.getBufferCapacity());
        this.bidSelectionDiscipline = new BidSelectionDiscipline(buffer);
        this.deviceSelectionDiscipline = new DeviceSelectionDiscipline(devices);
        this.bufferPlacementAndRejectionDispatcher = new BufferPlacementAndRejectionDispatcher(buffer);
    }

    /**
     * Инициализация источников.
     *
     * @param producerCount - количество источников в системе
     */
    private void initializeProducers(int producerCount) {
        if (producerCount < 1) {
            throw new IllegalArgumentException("Количество источников не может быть меньше 1");
        }

        IntStream.rangeClosed(1, producerCount).forEach(id -> producers.add(Producer.builder()
            .withId(id)
            .build()));
    }

    /**
     * Инициализация приборов.
     *
     * @param deviceCount - количество приборов в системе
     */
    private void initializeDevices(int deviceCount) {
        if (deviceCount < 1) {
            throw new IllegalArgumentException("Количество приборов не может быть меньше 1");
        }

        IntStream.rangeClosed(1, deviceCount).forEach(id -> devices.add(Device.builder()
            .withId(id)
            .withProcessingEndTime(0.0)
            .build()));
    }

    /**
     * Инициализация начальных заявок источниками.
     */
    private void initializeFirstBids() {
        AtomicReference<Double> initialTime = new AtomicReference<>(0.0);

        producers.forEach(producer -> {
            initialTime.set(initialTime.get() + poissonDistributionLaw.getTime(serviceSystemParams.getLambda()));

            Event generationEvent = Event.builder()
                .withId(eventIdGenerator.getNextEventId())
                .withBid(producer.generateBid())
                .withBidStatus(BidStatus.GENERATED)
                .withTime(initialTime.get())
                .withExternalDescription(BidStatus.GENERATED.getDescription() + " источником И" + producer.getId())
                .build();

            events.add(generationEvent);
            statistics.getBidLifecycleTimings().put(generationEvent.getBid().getName(), BidLifecycleTimings.builder()
                .withGenerationTime(initialTime.get())
                .build());
            statistics.getProducerStatistics().get(producer.getId()).incrementGeneratedBidsCount();
        });
    }

    /**
     * Выполнение следующего шага в пошаговом методе.
     */
    public Optional<Event> performNextStep() {
        if (events.isEmpty()) {
            return Optional.empty();
        }

        Event currentEvent = events.first();
        events.remove(currentEvent);

        handleEvent(currentEvent);
        return Optional.of(currentEvent);
    }

    /**
     * Обработка события.
     *
     * @param currentEvent - событие для обработки
     */
    private void handleEvent(Event currentEvent) {
        switch (currentEvent.getBidStatus()) {
            case GENERATED -> placeBidToBufferAndGenerateNewBid(currentEvent);
            case PLACED_IN_BUFFER, COMPLETED -> selectBidFromBufferAndSendToDevice(currentEvent);
        }
    }

    /**
     * Обработка события со статусом GENERATED.
     *
     * @param event - событие для обработки.
     */
    private void placeBidToBufferAndGenerateNewBid(Event event) {
        double newEventTime = event.getTime() + poissonDistributionLaw.getTime(serviceSystemParams.getLambda());
        if (newEventTime < serviceSystemParams.getMaxSimulationTime()) {
            Event generationEvent = Event.builder()
                .withId(eventIdGenerator.getNextEventId())
                .withBid(event.getBid().getProducer().generateBid())
                .withBidStatus(BidStatus.GENERATED)
                .withTime(newEventTime)
                .withExternalDescription(BidStatus.GENERATED.getDescription()
                    + " источником И" + event.getBid().getProducer().getId())
                .build();

            events.add(generationEvent);
            statistics.getProducerStatistics().get(event.getBid().getProducer().getId()).incrementGeneratedBidsCount();
            statistics.getBidLifecycleTimings().put(generationEvent.getBid().getName(), BidLifecycleTimings.builder()
                .withGenerationTime(newEventTime)
                .build());
            events.addAll(bufferPlacementAndRejectionDispatcher.placeBidInBuffer(event.getBid(), event.getTime()));
        }
    }

    /**
     * Обработка события со статусом PLACED_IN_BUFFER.
     *
     * @param event - событие для обработки.
     */
    private void selectBidFromBufferAndSendToDevice(Event event) {
        deviceSelectionDiscipline.findFreeDevice(event.getTime()).ifPresent(device -> {
            Pair<Optional<Bid>, Integer> bidToProcess = bidSelectionDiscipline.selectNextBid();
            if (bidToProcess.getLeft().isEmpty()) {
                return;
            }

            double processingEndTime = event.getTime() + uniformDistributionLaw
                .getTime(serviceSystemParams.getMinDeviceProcessingTime(),
                    serviceSystemParams.getMaxDeviceProcessingTime());
            device.setProcessingEndTime(processingEndTime);

            events.addAll(List.of(Event.builder()
                .withId(eventIdGenerator.getNextEventId())
                .withBid(bidToProcess.getLeft().get())
                .withBidStatus(BidStatus.ON_DEVICE)
                .withTime(event.getTime())
                .withExternalDescription(BidStatus.ON_DEVICE.getDescription() + " П" + device.getId())
                .withBufferIndex(bidToProcess.getRight())
                .withDeviceNumber(device.getId())
                .withEndProcessingTime(processingEndTime)
                .build(), Event.builder()
                .withId(eventIdGenerator.getNextEventId())
                .withBid(bidToProcess.getLeft().get())
                .withBidStatus(BidStatus.COMPLETED)
                .withDeviceNumber(device.getId())
                .withExternalDescription(BidStatus.COMPLETED.getDescription() + " прибором П" + device.getId())
                .withTime(processingEndTime)
                .build()));

            BidLifecycleTimings timings = statistics.getBidLifecycleTimings().get(bidToProcess.getLeft().get().getName());
            statistics.getProducerStatistics().get(bidToProcess.getLeft().get().getProducer().getId())
                .addTotalBidsInSystemTime(processingEndTime - timings.getGenerationTime());
            statistics.getProducerStatistics().get(bidToProcess.getLeft().get().getProducer().getId())
                .addTotalBidsWaitingTime(event.getTime() - timings.getGenerationTime());
            statistics.getProducerStatistics().get(bidToProcess.getLeft().get().getProducer().getId())
                .addTotalBidsProcessingTime(processingEndTime - event.getTime());

            statistics.getDeviceWorkTime().put(device.getId(),
                statistics.getDeviceWorkTime().get(device.getId()) + processingEndTime - event.getTime());

            statistics.getBidLifecycleTimings().remove(bidToProcess.getLeft().get().getName());
        });
    }
}
