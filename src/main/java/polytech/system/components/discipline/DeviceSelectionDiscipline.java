package polytech.system.components.discipline;

import lombok.Data;
import polytech.system.components.device.Device;

import java.util.List;
import java.util.Optional;

/**
 * Дисциплина выбора прибора для обслуживания заявки (Д2П1).
 */
@Data
public class DeviceSelectionDiscipline {
    /**
     * Приборы, работающие в СМО.
     */
    private final List<Device> devices;

    /**
     * Найти самый приоритетный свободный прибор.
     *
     * @param time - время, в которое мы ищем свободный прибор
     * @return самый приоритетный свободный прибор
     */
    public Optional<Device> findFreeDevice(double time) {
        return devices.stream().filter(device -> device.isFree(time)).findFirst();
    }
}
