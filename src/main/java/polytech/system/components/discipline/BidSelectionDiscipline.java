package polytech.system.components.discipline;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import polytech.system.components.bid.Bid;
import polytech.system.components.buffer.Buffer;

import java.util.Optional;

/**
 * Дисциплина выбора заявки из буфера (Д2Б5).
 */
@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
public class BidSelectionDiscipline {
    /**
     * Буфер.
     */
    private final Buffer buffer;

    /**
     * Самый приоритетный пакет заявок.
     */
    private int highestPriorityBidPackageNumber;

    public BidSelectionDiscipline(Buffer buffer) {
        this.buffer = buffer;
        highestPriorityBidPackageNumber = -1;
    }

    /**
     * Выбор заявки из буфера согласно дисциплине Д2Б5.
     *
     * @return выбранная заявка.
     */
    public Pair<Optional<Bid>, Integer> selectNextBid() {
        if (buffer.isEmpty()) {
            return new ImmutablePair<>(Optional.empty(), 0);
        }

        if (highestPriorityBidPackageNumber == -1) {
            initializeHighestPriorityBidPackageNumber();
        }

        for (int i = 0; i < buffer.getBids().length; i++) {
            if (buffer.getBids()[i] != null
                && buffer.getBids()[i].getProducer().getId() == highestPriorityBidPackageNumber) {
                Bid bidToReturn = Bid.builder()
                    .withName(buffer.getBids()[i].getName())
                    .withProducer(buffer.getBids()[i].getProducer())
                    .build();

                buffer.getBids()[i] = null;
                buffer.decrementNumberOfBidsInBuffer();
                return new ImmutablePair<>(Optional.of(bidToReturn), i);
            }
        }

        initializeHighestPriorityBidPackageNumber();
        return selectNextBid();
    }

    /**
     * Вычисление самого приоритетного пакета заявок.
     */
    private void initializeHighestPriorityBidPackageNumber() {
        int currentHighestPriorityBidPackageNumber = Integer.MAX_VALUE;
        for (var bid : buffer.getBids()) {
            if (bid != null) {
                currentHighestPriorityBidPackageNumber =
                    Math.min(currentHighestPriorityBidPackageNumber, bid.getProducer().getId());
            }
        }

        highestPriorityBidPackageNumber = currentHighestPriorityBidPackageNumber;
    }
}
