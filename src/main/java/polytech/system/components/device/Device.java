package polytech.system.components.device;

import lombok.Builder;
import lombok.Data;

/**
 * Прибор.
 */
@Data
@Builder(setterPrefix = "with")
public class Device {
    /**
     * Идентификатор.
     */
    private final int id;

    /**
     * Время, в которое закончится обработка заявки.
     */
    private double processingEndTime;

    /**
     * Свободен ли прибор в момент времени time
     *
     * @param time - текущее время
     * @return -7 Джоулей
     */
    public boolean isFree(double time) {
        return time >= processingEndTime;
    }
}
