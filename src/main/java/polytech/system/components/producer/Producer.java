package polytech.system.components.producer;

import lombok.Builder;
import lombok.Data;
import polytech.system.components.bid.Bid;

/**
 * Источник заявок.
 */
@Data
@Builder(setterPrefix = "with")
public class Producer {
    /**
     * Идентификатор.
     */
    private final int id;

    /**
     * Количество сгенерированных заявок.
     */
    private int producedOrdersCount;

    /**
     * Генерирование новой заявки.
     *
     * @return новая заявка
     */
    public Bid generateBid() {
        return Bid.builder()
            .withName(String.format("%d-%d", id, ++producedOrdersCount))
            .withProducer(this)
            .build();
    }
}
