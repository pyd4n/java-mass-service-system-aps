package polytech.system.components.event;

import lombok.Builder;
import lombok.Data;
import polytech.system.components.bid.Bid;
import polytech.system.components.bid.BidStatus;

/**
 * Событие.
 */
@Data
@Builder(setterPrefix = "with")
public class Event {

    /**
     * Идентификатор.
     */
    private final int id;

    /**
     * Заявка.
     */
    private final Bid bid;

    /**
     * Статус заявки.
     */
    private final BidStatus bidStatus;

    /**
     * Время, в которое произошло событие.
     */
    private final double time;

    /**
     * Дополненный статус заявки (на основании bidStatus, но с
     * добавлением информации о приборах и источниках)
     */
    private String externalDescription;

    /**
     * Место в буфере, на которое была установлена заявка
     * или из которого заявка ушла на прибор или в отказ.
     */
    private int bufferIndex;

    /**
     * Номер прибора, на который ушла заявка.
     */
    private int deviceNumber;

    /**
     * Время окончания обработки заявки прибором.
     */
    private double endProcessingTime;
}
