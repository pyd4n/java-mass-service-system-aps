package polytech.system.components.event;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс, отвечающий за выдачу id для event'ов.
 */
public class EventIdGenerator {
    /**
     * Единственный экземпляр класса.
     */
    private static EventIdGenerator instance;

    /**
     * Приватный конструктор.
     */
    private EventIdGenerator() {
    }

    /**
     * Статический метод для получения экземпляра класса.
     * @return экземпляр класса
     */
    public static EventIdGenerator getInstance() {
        if (instance == null) {
            instance = new EventIdGenerator();
        }

        return instance;
    }

    /**
     * Генератор id'шников.
     */
    private final AtomicInteger generator = new AtomicInteger();

    public int getNextEventId() {
        return generator.incrementAndGet();
    }
}
