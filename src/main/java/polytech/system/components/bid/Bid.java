package polytech.system.components.bid;

import lombok.Builder;
import lombok.Data;
import polytech.system.components.producer.Producer;

/**
 * Заявка.
 */
@Data
@Builder(setterPrefix = "with")
public class Bid {
    /**
     * Порядковый номер в виде номера источника и номера
     * заявки от этого источника, разделенные дефисом.
     */
    private final String name;

    /**
     * Источник, сгенерировавший эту заявку.
     */
    private Producer producer;
}
