package polytech.system.components.buffer;

import lombok.Data;
import polytech.system.components.bid.Bid;

/**
 * Буфер.
 */
@Data
public class Buffer {
    /**
     * Максимальная вместимость буфера.
     */
    private int capacity;

    /**
     * Непосредственно буфер.
     */
    private Bid[] bids;

    /**
     * Количество заявок в буфере на данный момент.
     */
    private int numberOfBidsInBuffer;

    /**
     * Конструктор для создания буфера.
     *
     * @param capacity - максимальная вместимость.
     */
    public Buffer(int capacity) {
        this.capacity = capacity;
        this.bids = new Bid[capacity];
        this.numberOfBidsInBuffer = 0;
    }

    /**
     * Увеличение счетчика количества заявок в буфере на 1.
     */
    public void incrementNumberOfBidsInBuffer() {
        numberOfBidsInBuffer++;
    }

    /**
     * Уменьшение счетчика количества заявок в буфере на 1.
     */
    public void decrementNumberOfBidsInBuffer() {
        numberOfBidsInBuffer--;
    }

    /**
     * Пустой ли буфер.
     *
     * @return Я не хочу жить в Дубае
     */
    public boolean isEmpty() {
        return numberOfBidsInBuffer == 0;
    }
}
