package polytech.system.components.buffer;

import lombok.Data;
import polytech.statistics.StatisticsHolder;
import polytech.system.components.bid.Bid;
import polytech.system.components.bid.BidStatus;
import polytech.system.components.event.Event;
import polytech.system.components.event.EventIdGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Диспетчер постановки заявок в буфер по кольцу (Д10З1)
 * и отказа под указателем (Д10О1).
 */
@Data
public class BufferPlacementAndRejectionDispatcher {
    /**
     * Буфер, в который устанавливаются заявки.
     */
    private final Buffer buffer;

    /**
     * Указатель.
     */
    private int pointer = 0;

    /**
     * Статистика СМО.
     */
    private final StatisticsHolder statistics = StatisticsHolder.getInstance();

    /**
     * Генератор id'шников для event'ов.
     */
    private final EventIdGenerator eventIdGenerator = EventIdGenerator.getInstance();

    /**
     * Установить заявку в буфер.
     *
     * @param bid               - заявка для постановки в буфер
     * @param bidGenerationTime - время генерации заявки
     * @return событие установки заявки в буфер или вылета её в отказ
     */
    public List<Event> placeBidInBuffer(Bid bid, double bidGenerationTime) {
        List<Event> placementResultEvents = new ArrayList<>();
        if (buffer.getNumberOfBidsInBuffer() == buffer.getCapacity()) {
            Bid bidToReject = Bid.builder()
                .withName(buffer.getBids()[pointer].getName())
                .withProducer(buffer.getBids()[pointer].getProducer())
                .build();

            buffer.getBids()[pointer] = null;
            buffer.decrementNumberOfBidsInBuffer();
            placementResultEvents.add(Event.builder()
                .withId(eventIdGenerator.getNextEventId())
                .withBid(bidToReject)
                .withBidStatus(BidStatus.REJECTED)
                .withBufferIndex(pointer)
                .withTime(bidGenerationTime)
                .build());

            statistics.getProducerStatistics().get(bidToReject.getProducer().getId()).incrementRejectedBidsCount();
            statistics.getBidLifecycleTimings().remove(bidToReject.getName());
        }

        int index = addBidToBuffer(bid);
        placementResultEvents.add(Event.builder()
            .withId(eventIdGenerator.getNextEventId())
            .withBid(bid)
            .withBidStatus(BidStatus.PLACED_IN_BUFFER)
            .withTime(bidGenerationTime)
            .withBufferIndex(index)
            .build());

        return placementResultEvents;
    }

    /**
     * Добавление заявки в буфер согласно кольцевой дисциплине.
     *
     * @param bid - заявка для добавления
     * @return индекс в буфере, на который была поставлена заявка
     */
    private int addBidToBuffer(Bid bid) {
        while (buffer.getBids()[pointer] != null) {
            incrementPointer();
        }

        int pointerToReturn = pointer;
        buffer.getBids()[pointer] = bid;
        incrementPointer();
        buffer.incrementNumberOfBidsInBuffer();
        return pointerToReturn;
    }

    /**
     * Перемещение указателя на следующую позицию.
     */
    private void incrementPointer() {
        pointer = (pointer + 1) % buffer.getBids().length;
    }
}
