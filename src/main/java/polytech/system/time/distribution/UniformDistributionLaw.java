package polytech.system.time.distribution;

/**
 * Равномерный закон распределения.
 */
public class UniformDistributionLaw {
    /**
     * Получение времени согласно Равномерному закону распределения.
     *
     * @param minimum - нижняя граница
     * @param maximum - верхняя граница
     * @return время
     */
    public Double getTime(double minimum, double maximum) {
        return Math.random() * (maximum - minimum) + minimum;
    }
}
