package polytech.system.time.distribution;

/**
 * Пуассоновский закон распределения.
 */
public class PoissonDistributionLaw {
    /**
     * Получение времени согласно Пуассоновскому закону распределения.
     *
     * @return время.
     */
    public double getTime(double lambda) {
        return (-1.0 / lambda) * Math.log(Math.random());
    }
}
