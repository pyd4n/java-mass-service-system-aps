package polytech.system;

import lombok.Builder;
import lombok.Data;

/**
 * Входные параметры для создания СМО.
 */
@Data
@Builder(setterPrefix = "with")
public class MassServiceSystemParams {
    /**
     * Количество источников заявок.
     */
    private final int producerCount;

    /**
     * Максимальная вместимость буфера.
     */
    private final int bufferCapacity;

    /**
     * Количество приборов.
     */
    private final int deviceCount;

    /**
     * Лямбда для Пуассоновского закона распределения.
     */
    private final double lambda;

    /**
     * Максимальное время работы СМО.
     */
    private final double maxSimulationTime;

    /**
     * Минимальное время обработки заявки прибором.
     */
    private final double minDeviceProcessingTime;

    /**
     * Максимальное время обработки заявки прибором.
     */
    private final double maxDeviceProcessingTime;
}
