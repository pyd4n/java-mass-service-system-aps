package polytech.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Собранные статистические данные по источнику.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class ProducerStatistics {
    /**
     * Количество сгенерированных заявок.
     */
    private int generatedBidsCount;

    /**
     * Количество заявок, отказанных в обслуживании.
     */
    private int rejectedBidsCount;

    /**
     * Общее количество времени пребывания всех
     * сгенерированных заявок в системе.
     */
    private double totalBidsInSystemTime;

    /**
     * Общее количество времени ожидания всех
     * сгенерированных заявок в системе.
     */
    private double totalBidsWaitingTime;

    /**
     * Сумма квадратов времени ожидания всех
     * сгенерированных заявок в системе.
     */
    private double squaredTotalBidsWaitingTime;

    /**
     * Общее количество времени обслуживания
     * всех сгенерированных заявок.
     */
    private double totalBidsProcessingTime;

    /**
     * Сумма квадратов времени обслуживания
     * всех сгенерированных заявок.
     */
    private double squaredTotalBidsProcessingTime;

    /**
     * Инкремент общего количества сгенерированных заявок.
     */
    public void incrementGeneratedBidsCount() {
        generatedBidsCount++;
    }

    /**
     * Инкремент общего количества отказанных заявок.
     */
    public void incrementRejectedBidsCount() {
        rejectedBidsCount++;
    }

    public void addTotalBidsInSystemTime(double time) {
        totalBidsInSystemTime += time;
    }

    public void addTotalBidsWaitingTime(double time) {
        totalBidsWaitingTime += time;
        squaredTotalBidsWaitingTime += time * time;
    }

    public void addTotalBidsProcessingTime(double time) {
        totalBidsProcessingTime += time;
        squaredTotalBidsProcessingTime += time * time;
    }
}
