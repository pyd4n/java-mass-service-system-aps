package polytech.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Временные промежутки заявки.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class BidLifecycleTimings {
    /**
     * Время генерации.
     */
    private double generationTime;
}
