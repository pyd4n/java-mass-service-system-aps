package polytech.statistics;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Вся статистика системы.
 */
@Data
public class StatisticsHolder {
    private static StatisticsHolder instance;

    private StatisticsHolder() {
    }

    public static StatisticsHolder getInstance() {
        if (instance == null) {
            instance = new StatisticsHolder();
        }

        return instance;
    }

    /**
     * Статистика каждого источника.
     */
    private Map<Integer, ProducerStatistics> producerStatistics;

    /**
     * Сколько времени проработал каждый прибор.
     */
    private Map<Integer, Double> deviceWorkTime = new HashMap<>();

    /**
     * Временные промежутки всех заявок в системе.
     */
    private Map<String, BidLifecycleTimings> bidLifecycleTimings = new HashMap<>();
}
