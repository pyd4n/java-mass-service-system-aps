package polytech.gui.params;

import lombok.Data;

import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Класс, отвечающий за информацию о выводимом тексте для ввода
 * параметра, поле для ввода и классе, в который текст должен быть сконвертирован.
 */
@Data
public class ParamInputParseHolder {
    /**
     * Текст перед текстовым полем.
     */
    private final JLabel jLabel;

    /**
     * Поле для ввода параметра.
     */
    private final JTextField jTextField;

    /**
     * Статический метод получения объекта.
     *
     * @param label      - Текст перед текстовым полем
     * @param textField  - Поле для ввода параметра
     *                   введенное значение
     * @return объект с проинициализированными полями
     */
    public static ParamInputParseHolder of(JLabel label, JTextField textField) {
        return new ParamInputParseHolder(label, textField);
    }
}
