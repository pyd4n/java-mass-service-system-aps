package polytech.gui.params;

import polytech.gui.main.MassServiceSystemFrame;
import polytech.system.MassServiceSystemParams;

import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Класс, отвечающий за отображение формы ввода параметров СМО.
 */
public class ParamsInputFrame extends JFrame {
    /**
     * Ширина окна в пикселях.
     */
    private static final int WIDTH_PX = 800;

    /**
     * Высота окна в пикселях.
     */
    private static final int HEIGHT_PX = 600;

    /**
     * Высота полей ввода и отступов между ними.
     */
    private static final int ELEMENT_HEIGHT = HEIGHT_PX / 18;

    /**
     * Отступ по бокам, в процентах.
     */
    private static final double HORIZONTAL_OFFSET_PERCENT = 5;

    /**
     * Ширина кнопки ввода в пикселях.
     */
    private static final int BUTTON_WIDTH = WIDTH_PX / 5;

    /**
     * Кнопка отправки формы ввода параметров.
     */
    private final JButton paramsSubmitButton = new JButton("Ввести параметры!");

    /**
     * Все необходимые поля для ввода.
     */
    private final NavigableMap<Integer, ParamInputParseHolder> paramsInputLabelsAndFields =
        new TreeMap<>(Integer::compareTo);

    {
        paramsInputLabelsAndFields.putAll(Map.of(
            1, ParamInputParseHolder.of(new JLabel("Введите количество предприятий: "), new JTextField("10")),
            2, ParamInputParseHolder.of(new JLabel("Введите количество столиков для ожидания: "), new JTextField("5")),
            3, ParamInputParseHolder.of(new JLabel("Введите количество кофемашин: "), new JTextField("15")),
            4, ParamInputParseHolder.of(new JLabel("Введите лямбду для Пуассоновского закона: "), new JTextField("0.2")),
            5, ParamInputParseHolder.of(new JLabel("Введите максимальное время работы кофейни: "), new JTextField("1440")),
            6, ParamInputParseHolder.of(new JLabel("Введите минимальное время приготовления кофе: "), new JTextField("5")),
            7, ParamInputParseHolder.of(new JLabel("Введите максимальное время приготовления кофе: "), new JTextField("10"))
        ));
    }

    /**
     * Конструктор, создающий все необходимые поля и кнопки для ввода параметров.
     */
    public ParamsInputFrame() {
        setTitle("Введите параметры для работы СМО");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(WIDTH_PX, HEIGHT_PX);
        setResizable(false);
        setVisible(true);

        placeAndConfigureAddElements();
    }

    private void placeAndConfigureAddElements() {
        for (var input : paramsInputLabelsAndFields.entrySet()) {
            input.getValue().getJLabel().setBounds((int) (WIDTH_PX * HORIZONTAL_OFFSET_PERCENT / 100.0),
                ELEMENT_HEIGHT * (input.getKey() * 2 - 1),
                (int) ((50.0 - HORIZONTAL_OFFSET_PERCENT) / 100.0 * WIDTH_PX), ELEMENT_HEIGHT);

            input.getValue().getJTextField().setBounds(WIDTH_PX / 2,
                ELEMENT_HEIGHT * (input.getKey() * 2 - 1),
                (int) ((50.0 - HORIZONTAL_OFFSET_PERCENT) / 100.0 * WIDTH_PX), ELEMENT_HEIGHT);

            add(input.getValue().getJLabel());
            add(input.getValue().getJTextField());
        }

        paramsSubmitButton.setBounds((WIDTH_PX - BUTTON_WIDTH) / 2,
            ELEMENT_HEIGHT * ((paramsInputLabelsAndFields.lastKey() + 1) * 2 - 1),
            BUTTON_WIDTH, ELEMENT_HEIGHT);
        paramsSubmitButton.addActionListener(this::submitParams);
        add(paramsSubmitButton);
    }

    public void submitParams(ActionEvent e) {
        dispose();
        new MassServiceSystemFrame(MassServiceSystemParams.builder()
            .withProducerCount(Integer.parseInt(paramsInputLabelsAndFields.get(1).getJTextField().getText()))
            .withBufferCapacity(Integer.parseInt(paramsInputLabelsAndFields.get(2).getJTextField().getText()))
            .withDeviceCount(Integer.parseInt(paramsInputLabelsAndFields.get(3).getJTextField().getText()))
            .withLambda(Double.parseDouble(paramsInputLabelsAndFields.get(4).getJTextField().getText()))
            .withMaxSimulationTime(Double.parseDouble(paramsInputLabelsAndFields.get(5).getJTextField().getText()))
            .withMinDeviceProcessingTime(Double.parseDouble(paramsInputLabelsAndFields.get(6).getJTextField().getText()))
            .withMaxDeviceProcessingTime(Double.parseDouble(paramsInputLabelsAndFields.get(7).getJTextField().getText()))
            .build());
    }
}
