package polytech.gui.statistics;

import polytech.statistics.ProducerStatistics;
import polytech.statistics.StatisticsHolder;
import polytech.system.MassServiceSystemParams;

import java.awt.Font;
import java.text.DecimalFormat;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Окно со статистикой СМО.
 */
public class TotalStatisticsFrame extends JFrame {
    private static final DecimalFormat TIME_FORMAT = new DecimalFormat("#.#######");

    /**
     * Ширина окна в пикселях.
     */
    private static final int WIDTH_PX = 1800;

    /**
     * Высота окна в пикселях.
     */
    private static final int HEIGHT_PX = 1000;

    /**
     * Названия колонок для таблицы со статистикой по источникам.
     */
    private static final List<String> STATISTICS_TABLE_COLUMN_NAMES = List.of(
        "Номер источника",
        "Количество сгенерированных заявок",
        "Вероятность отказа",
        "Среднее время пребывания заявок",
        "Среднее время ожидания заявок",
        "Среднее время обслуживания заявок",
        "Дисперсия среднего времени ожидания",
        "Дисперсия среднего времени обслуживания"
    );

    /**
     * Названия колонок для таблицы со статистикой по приборам.
     */
    private static final List<String> DEVICES_TABLE_COLUMN_NAMES = List.of(
        "Номер прибора",
        "Коэффициент использования"
    );

    /**
     * Таблица со статистикой по источникам.
     */
    private final JTable producersStatisticsTable = new JTable(new Object[][]{}, STATISTICS_TABLE_COLUMN_NAMES.toArray());

    /**
     * Модель для динамического добавления строк в таблицу со статистикой по источникам.
     */
    private final DefaultTableModel producersStatisticsTableModel = new DefaultTableModel(0, STATISTICS_TABLE_COLUMN_NAMES.size());

    /**
     * Прокрутка для таблицы со статистикой по источникам.
     */
    private final JScrollPane producersStatisticsTableScrollPane = new JScrollPane(producersStatisticsTable);

    /**
     * Таблица со статистикой по приборам.
     */
    private final JTable devicesStatisticsTable = new JTable(new Object[][]{}, DEVICES_TABLE_COLUMN_NAMES.toArray());

    /**
     * Модель для динамического добавления строк в таблицу со статистикой по приборам.
     */
    private final DefaultTableModel devicesStatisticsTableModel = new DefaultTableModel(0, DEVICES_TABLE_COLUMN_NAMES.size());

    /**
     * Прокрутка для таблицы со статистикой по приборам.
     */
    private final JScrollPane devicesStatisticsTableScrollPane = new JScrollPane(devicesStatisticsTable);

    /**
     * Статистика СМО.
     */
    private final StatisticsHolder statistics = StatisticsHolder.getInstance();

    /**
     * Параметры СМО.
     */
    private final MassServiceSystemParams serviceSystemParams;

    /**
     * Конструктор с инициализацией окна.
     */
    public TotalStatisticsFrame(MassServiceSystemParams serviceSystemParams) {
        this.serviceSystemParams = serviceSystemParams;
        setTitle("Результаты работы СМО");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        placeAndConfigureStatisticsTable();
        placeAndConfigureDevicesTable();
        setSize(WIDTH_PX, HEIGHT_PX);
        setResizable(false);
        setVisible(true);

        System.out.println("Лямбда = " + serviceSystemParams.getLambda() + ", средняя вероятность отказа = "
        + TIME_FORMAT.format(findAvgRejectionProbability(statistics) / statistics.getProducerStatistics().size())
        + ", средняя загрузка кофемашин = " + findAvgDevicesWorkTimeCoefficient(statistics)
        + ", среднее время ожидания клиента = " + findAvgClientWaitTime(statistics));
    }

    /**
     * Расчет средней вероятности отказа по всем источникам.
     *
     * @param statistics статистика всех источником
     * @return средняя вероятность отказа по всем источникам
     */
    private double findAvgRejectionProbability(StatisticsHolder statistics) {
        double rejectionProbabilitySum = 0;
        for (var producerStats : statistics.getProducerStatistics().entrySet()) {
            rejectionProbabilitySum += (double) producerStats.getValue().getRejectedBidsCount() / producerStats.getValue().getGeneratedBidsCount();
        }

        return rejectionProbabilitySum;
    }

    /**
     * Расчет среднего коэффициента загруженности приборов.
     *
     * @param statistics статистика всех приборов
     * @return средний коэффициент загруженности приборов
     */
    private double findAvgDevicesWorkTimeCoefficient(StatisticsHolder statistics) {
        double avgDevicesWorkTimeCoefficient = 0;
        for (var deviceStats : statistics.getDeviceWorkTime().entrySet()) {
            avgDevicesWorkTimeCoefficient += deviceStats.getValue();
        }

        return avgDevicesWorkTimeCoefficient / (serviceSystemParams.getMaxSimulationTime() * statistics.getDeviceWorkTime().size());
    }

    /**
     * Расчет среднего времени ожидания клиента.
     *
     * @param statistics статистика всех приборов
     * @return среднее время ожидания клиента
     */
    private double findAvgClientWaitTime(StatisticsHolder statistics) {
        double avgClientWaitTime = 0;
        for (var producerStats : statistics.getProducerStatistics().entrySet()) {
            avgClientWaitTime += producerStats.getValue().getTotalBidsInSystemTime() / producerStats.getValue().getGeneratedBidsCount();
        }

        return avgClientWaitTime / serviceSystemParams.getProducerCount();
    }

    /**
     * Установка и заполнение таблицы со статистикой по источникам.
     */
    private void placeAndConfigureStatisticsTable() {
        producersStatisticsTableScrollPane.setVisible(true);
        producersStatisticsTableModel.setColumnIdentifiers(STATISTICS_TABLE_COLUMN_NAMES.toArray());
        producersStatisticsTable.setModel(producersStatisticsTableModel);
        producersStatisticsTable.setFont(new Font("Serif", Font.PLAIN, 24));
        producersStatisticsTable.setRowHeight(producersStatisticsTable.getRowHeight() + 10);
        producersStatisticsTableScrollPane.setBounds(0, 0, WIDTH_PX, HEIGHT_PX / 2 - 50);
        getContentPane().add(producersStatisticsTableScrollPane);

        NavigableMap<Integer, ProducerStatistics> producerStatistics = new TreeMap<>(Integer::compareTo);
        producerStatistics.putAll(statistics.getProducerStatistics());

        for (var producerStats : producerStatistics.entrySet()) {
            ProducerStatistics value = producerStats.getValue();
            producersStatisticsTableModel.addRow(List.of(
                producerStats.getKey(),
                value.getGeneratedBidsCount(),
                TIME_FORMAT.format((double) value.getRejectedBidsCount() / value.getGeneratedBidsCount()),
                TIME_FORMAT.format(value.getTotalBidsInSystemTime() / value.getGeneratedBidsCount()),
                TIME_FORMAT.format(value.getTotalBidsWaitingTime() / (value.getGeneratedBidsCount() - value.getRejectedBidsCount())),
                TIME_FORMAT.format(value.getTotalBidsProcessingTime() / (value.getGeneratedBidsCount() - value.getRejectedBidsCount())),
                TIME_FORMAT.format(getWaitingDispersion(value)),
                TIME_FORMAT.format(getProcessingDispersion(value))
            ).toArray());
        }
    }

    /**
     * Установка и заполнение таблицы со статистикой по приборам.
     */
    private void placeAndConfigureDevicesTable() {
        devicesStatisticsTable.setVisible(true);
        devicesStatisticsTableModel.setColumnIdentifiers(DEVICES_TABLE_COLUMN_NAMES.toArray());
        devicesStatisticsTable.setModel(devicesStatisticsTableModel);
        devicesStatisticsTable.setFont(new Font("Serif", Font.PLAIN, 24));
        devicesStatisticsTable.setRowHeight(devicesStatisticsTable.getRowHeight() + 10);
        devicesStatisticsTableScrollPane.setBounds(0, HEIGHT_PX / 2, WIDTH_PX / 3, HEIGHT_PX / 2 - 50);
        getContentPane().add(devicesStatisticsTableScrollPane);

        NavigableMap<Integer, Double> devicesStatistics = new TreeMap<>(Integer::compareTo);
        devicesStatistics.putAll(statistics.getDeviceWorkTime());

        for (var deviceStats : devicesStatistics.entrySet()) {
            devicesStatisticsTableModel.addRow(List.of(
                deviceStats.getKey(),
                TIME_FORMAT.format(deviceStats.getValue() / serviceSystemParams.getMaxSimulationTime())
            ).toArray());
        }
    }

    private double getWaitingDispersion(ProducerStatistics value) {
        return (value.getSquaredTotalBidsWaitingTime() / value.getGeneratedBidsCount() -
            Math.pow(value.getTotalBidsWaitingTime() / value.getGeneratedBidsCount(), 2));
    }

    private double getProcessingDispersion(ProducerStatistics value) {
        return (value.getSquaredTotalBidsProcessingTime() / value.getGeneratedBidsCount() -
            Math.pow(value.getTotalBidsProcessingTime() / value.getGeneratedBidsCount(), 2));
    }
}
