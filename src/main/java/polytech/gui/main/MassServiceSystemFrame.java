package polytech.gui.main;

import polytech.gui.statistics.TotalStatisticsFrame;
import polytech.system.MassServiceSystemController;
import polytech.system.MassServiceSystemParams;
import polytech.system.components.bid.BidStatus;
import polytech.system.components.buffer.BufferCellStatus;
import polytech.system.components.event.Event;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MassServiceSystemFrame extends JFrame {
    private static final DecimalFormat TIME_FORMAT = new DecimalFormat("#.#####");

    /**
     * Ширина окна в пикселях.
     */
    private static final int WIDTH_PX = 1800;

    /**
     * Высота окна в пикселях.
     */
    private static final int HEIGHT_PX = 1000;


    /**
     * Ширина таблицы событий.
     */
    private static final int EVENT_TABLE_WIDTH_PERCENT = 70;

    /**
     * Координата по X таблицы событий.
     */
    private static final int EVENTS_TABLE_X_COORDINATE_PX = (int) ((100.0 - EVENT_TABLE_WIDTH_PERCENT) / 100.0 * WIDTH_PX) - 15;

    /**
     * Ширина кнопок, в пикселях.
     */
    private static final int BUTTONS_WIDTH = WIDTH_PX / 10;

    /**
     * Высота кнопок, в пикселях.
     */
    private static final int BUTTONS_HEIGHT = HEIGHT_PX / 20;

    /**
     * Отступ по краям у кнопок, в пикселях.
     */
    private static final int BUTTONS_OFFSET_PX = WIDTH_PX / 70;

    /**
     * Названия колонок для таблицы событий.
     */
    private static final List<String> EVENTS_TABLE_COLUMN_NAMES = List.of(
        "№",
        "Время",
        "Заявка",
        "Событие",
        "Исполнено",
        "Отказано"
    );

    /**
     * Названия колонок для таблицы буфера.
     */
    private static final List<String> BUFFER_TABLE_COLUMN_NAMES = List.of(
        "Индекс",
        "Статус",
        "Заявка",
        "Указатель"
    );

    /**
     * Названия колонок для таблицы приборов.
     */
    private static final List<String> DEVICES_TABLE_COLUMN_NAMES = List.of(
        "Индекс",
        "Заявка на обслуживании",
        "Время окончания обслуживания"
    );

    /**
     * Названия колонок для таблицы приборов.
     */
    private static final List<String> PRODUCERS_TABLE_COLUMN_NAMES = List.of(
        "Индекс",
        "Номер заявки",
        "Время генерации"
    );

    /**
     * Номер текущего события для отображения в таблице.
     */
    private static int CURRENT_EVENT_NUMBER = 0;

    /**
     * Текущее количество выполненных заявок.
     */
    private static int CURRENT_COMPLETED_EVENTS_NUMBER = 0;

    /**
     * Текущее количество отказанных заявок.
     */
    private static int CURRENT_REJECTED_EVENTS_NUMBER = 0;

    /**
     * Таблица со всеми событиями в СМО.
     */
    private final JTable eventsTable = new JTable(new Object[][]{}, EVENTS_TABLE_COLUMN_NAMES.toArray());

    /**
     * Модель для динамического добавления строк в таблицу событий.
     */
    private final DefaultTableModel eventsTableModel = new DefaultTableModel(0, EVENTS_TABLE_COLUMN_NAMES.size());

    /**
     * Прокрутка для таблицы событий.
     */
    private final JScrollPane eventsTableScrollPane = new JScrollPane(eventsTable);

    /**
     * Таблица для представления буфера.
     */
    private final JTable bufferTable = new JTable(new Object[][]{}, BUFFER_TABLE_COLUMN_NAMES.toArray());

    /**
     * Модель для динамического обновления строк в таблице буфера.
     */
    private final DefaultTableModel bufferTableModel = new DefaultTableModel(0, BUFFER_TABLE_COLUMN_NAMES.size());

    /**
     * Прокрутка для таблицы буфера.
     */
    private final JScrollPane bufferTableScrollPane = new JScrollPane(bufferTable);

    /**
     * Таблица для предоставления приборов.
     */
    private final JTable devicesTable = new JTable(new Object[][]{}, DEVICES_TABLE_COLUMN_NAMES.toArray());

    /**
     * Модель для динамического обновления строк в таблице приборов.
     */
    private final DefaultTableModel devicesTableModel = new DefaultTableModel(0, DEVICES_TABLE_COLUMN_NAMES.size());

    /**
     * Прокрутка для таблицы приборов.
     */
    private final JScrollPane devicesTableScrollPane = new JScrollPane(devicesTable);

    /**
     * Таблица для предоставления источников.
     */
    private final JTable producersTable = new JTable(new Object[][]{}, PRODUCERS_TABLE_COLUMN_NAMES.toArray());

    /**
     * Модель для динамического обновления строк в таблице источников.
     */
    private final DefaultTableModel producersTableModel = new DefaultTableModel(0, PRODUCERS_TABLE_COLUMN_NAMES.size());

    /**
     * Прокрутка для таблицы источников.
     */
    private final JScrollPane producersTableScrollPane = new JScrollPane(producersTable);

    /**
     * Кнопка следующего шага для пошагового режима.
     */
    private final JButton nextStepButton = new JButton("Следующий шаг");

    /**
     * Кнопка для автоматического режима.
     */
    private final JButton autoModeButton = new JButton("Автоматический режим");

    /**
     * Кнопка для просмотра статистики.
     */
    private final JButton showStatisticsButton = new JButton("Посмотреть статистику");

    /**
     * Контроллер СМО.
     */
    private final MassServiceSystemController massServiceSystemController;

    /**
     * Конструктор
     *
     * @param serviceSystemParams - параметры системы
     */
    public MassServiceSystemFrame(MassServiceSystemParams serviceSystemParams) {
        massServiceSystemController = new MassServiceSystemController(serviceSystemParams);
        setTitle("Имитация работы СМО");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        placeAndConfigureEventsTable();
        placeAndConfigureBufferTable();
        placeAndConfigureDevicesTable();
        placeAndConfigureProducersTable();
        placeAndConfigureButtons();
        setSize(WIDTH_PX, HEIGHT_PX);
        setResizable(false);
        setVisible(true);
    }

    /**
     * Размещение и настройка таблицы событий.
     */
    private void placeAndConfigureEventsTable() {
        eventsTableScrollPane.setVisible(true);
        eventsTableModel.setColumnIdentifiers(EVENTS_TABLE_COLUMN_NAMES.toArray());
        eventsTable.setModel(eventsTableModel);
        eventsTable.setFont(new Font("Serif", Font.PLAIN, 24));
        eventsTable.setRowHeight(eventsTable.getRowHeight() + 10);
        eventsTableScrollPane.setBounds(EVENTS_TABLE_X_COORDINATE_PX, 0,
            (int) (EVENT_TABLE_WIDTH_PERCENT / 100.0 * WIDTH_PX), HEIGHT_PX - 40);
        getContentPane().add(eventsTableScrollPane);
    }

    /**
     * Размещение и настройка таблицы буфера.
     */
    private void placeAndConfigureBufferTable() {
        bufferTableScrollPane.setVisible(true);
        bufferTableModel.setColumnIdentifiers(BUFFER_TABLE_COLUMN_NAMES.toArray());
        bufferTable.setModel(bufferTableModel);
        bufferTable.setFont(new Font("Serif", Font.PLAIN, 24));
        bufferTable.setRowHeight(bufferTable.getRowHeight() + 10);
        bufferTableScrollPane.setBounds(0, 0, (int) (WIDTH_PX * (100.0 - EVENT_TABLE_WIDTH_PERCENT) / 100.0 - 15), 250);
        getContentPane().add(bufferTableScrollPane);

        for (int i = 0; i < massServiceSystemController.getServiceSystemParams().getBufferCapacity(); i++) {
            bufferTableModel.addRow(List.of((i + 1),
                BufferCellStatus.FREE.getDescription(), "", i == 0 ? "<=====" : "").toArray());
        }
    }

    /**
     * Размещение и настройка таблицы приборов.
     */
    private void placeAndConfigureDevicesTable() {
        devicesTableScrollPane.setVisible(true);
        devicesTableModel.setColumnIdentifiers(DEVICES_TABLE_COLUMN_NAMES.toArray());
        devicesTable.setModel(devicesTableModel);
        devicesTable.setFont(new Font("Serif", Font.PLAIN, 24));
        devicesTable.setRowHeight(devicesTable.getRowHeight() + 10);
        devicesTableScrollPane.setBounds(0, 260, (int) (WIDTH_PX * (100.0 - EVENT_TABLE_WIDTH_PERCENT) / 100.0 - 15), 250);
        getContentPane().add(devicesTableScrollPane);

        for (int i = 0; i < massServiceSystemController.getServiceSystemParams().getDeviceCount(); i++) {
            devicesTableModel.addRow(List.of((i + 1), "", "").toArray());
        }
    }

    /**
     * Размещение и настройка таблицы источников
     */
    private void placeAndConfigureProducersTable() {
        producersTableScrollPane.setVisible(true);
        producersTableModel.setColumnIdentifiers(PRODUCERS_TABLE_COLUMN_NAMES.toArray());
        producersTable.setModel(producersTableModel);
        producersTable.setFont(new Font("Serif", Font.PLAIN, 24));
        producersTable.setRowHeight(producersTable.getRowHeight() + 10);
        producersTableScrollPane.setBounds(0, 520, (int) (WIDTH_PX * (100.0 - EVENT_TABLE_WIDTH_PERCENT) / 100.0 - 15), 250);
        getContentPane().add(producersTableScrollPane);

        for (int i = 0; i < massServiceSystemController.getServiceSystemParams().getProducerCount(); i++) {
            producersTableModel.addRow(List.of((i + 1), "", "").toArray());
        }
    }

    /**
     * Размещение и настройка кнопок.
     */
    private void placeAndConfigureButtons() {
        nextStepButton.setBounds(BUTTONS_OFFSET_PX, HEIGHT_PX - 3 * BUTTONS_OFFSET_PX - BUTTONS_HEIGHT,
            BUTTONS_WIDTH, BUTTONS_HEIGHT);
        nextStepButton.addActionListener(this::nextStepButtonActionPerformed);
        add(nextStepButton);

        autoModeButton.setBounds(2 * BUTTONS_OFFSET_PX + BUTTONS_WIDTH, HEIGHT_PX - 3 * BUTTONS_OFFSET_PX - BUTTONS_HEIGHT,
            BUTTONS_WIDTH, BUTTONS_HEIGHT);
        autoModeButton.addActionListener(this::autoModeButtonActionPerformed);
        add(autoModeButton);

        showStatisticsButton.setBounds(2 * BUTTONS_OFFSET_PX + BUTTONS_WIDTH, HEIGHT_PX - 4 * BUTTONS_OFFSET_PX - 2 * BUTTONS_HEIGHT,
            BUTTONS_WIDTH, BUTTONS_HEIGHT);
        showStatisticsButton.addActionListener(this::showStatisticsButtonActionPerformed);
        add(showStatisticsButton);
    }

    /**
     * Метод выполнения пошагового режима.
     *
     * @param actionEvent - событие
     */
    private boolean nextStepButtonActionPerformed(ActionEvent actionEvent) {
        Optional<Event> performedEvent = massServiceSystemController.performNextStep();
        if (performedEvent.isPresent()) {
            Event event = performedEvent.get();
            eventsTableModel.addRow(List.of(++CURRENT_EVENT_NUMBER, TIME_FORMAT.format(event.getTime()),
                event.getBid().getName(), event.getExternalDescription() != null
                    ? event.getExternalDescription()
                    : event.getBidStatus().getDescription(),
                getCurrentCompletedEventsNumber(event), getCurrentRejectedEventsNumber(event)).toArray());

            switch (event.getBidStatus()) {
                case GENERATED -> redrawProducersTableOnGeneratedBidEvent(event);
                case PLACED_IN_BUFFER -> redrawBufferOnPlacedBidEvent(event);
                case ON_DEVICE -> {
                    redrawBufferOnDeviceBidEvent(event);
                    redrawDevicesOnDeviceBidEvent(event);
                }
                case REJECTED -> redrawBufferOnRejectedBidEvent(event);
                case COMPLETED -> redrawDevicesOnCompletedBidEvent(event);
            }
            return true;
        }

        return false;
    }

    /**
     * Получить количество выполненных заявок, учитывая event.
     *
     * @param event - событие
     * @return - количество выполненных заявок, учитывая event.
     */
    private int getCurrentCompletedEventsNumber(Event event) {
        if (event.getBidStatus().equals(BidStatus.COMPLETED)) {
            CURRENT_COMPLETED_EVENTS_NUMBER++;
        }

        return CURRENT_COMPLETED_EVENTS_NUMBER;
    }

    /**
     * Получить количество отказанных заявок, учитывая event.
     *
     * @param event - событие
     * @return - количество отказанных заявок, учитывая event
     */
    private int getCurrentRejectedEventsNumber(Event event) {
        if (event.getBidStatus().equals(BidStatus.REJECTED)) {
            CURRENT_REJECTED_EVENTS_NUMBER++;
        }

        return CURRENT_REJECTED_EVENTS_NUMBER;
    }

    /**
     * Перерисовка буфера в случае события на постановку.
     *
     * @param event - заявка, которую поставили в буфер
     */
    private void redrawBufferOnPlacedBidEvent(Event event) {
        bufferTableModel.removeRow(event.getBufferIndex());
        bufferTableModel.insertRow(event.getBufferIndex(), List.of(event.getBufferIndex() + 1,
            BufferCellStatus.HAS_BID.getDescription() + " " + event.getBid().getName(),
            event.getBid().getName(), "").toArray());

        for (int i = 0; i < bufferTableModel.getRowCount(); i++) {
            bufferTableModel.setValueAt("", i, BUFFER_TABLE_COLUMN_NAMES.size() - 1);
        }

        bufferTableModel.setValueAt("<=====",
            (event.getBufferIndex() + 1) % massServiceSystemController.getBuffer().getCapacity(),
            BUFFER_TABLE_COLUMN_NAMES.size() - 1);
    }

    /**
     * Перерисовка буфера в случае события на отправления на прибор.
     *
     * @param event - заявка, которая отправилась на прибор
     */
    private void redrawBufferOnDeviceBidEvent(Event event) {
        Object value = bufferTableModel.getValueAt(event.getBufferIndex(), 3);
        bufferTableModel.removeRow(event.getBufferIndex());
        bufferTableModel.insertRow(event.getBufferIndex(), List.of(event.getBufferIndex() + 1,
            BufferCellStatus.FREE.getDescription(), "", value).toArray());
    }

    /**
     * Перерисовка буфера в случае события отказа заявки.
     *
     * @param event - заявка, которая вылетела в отказ
     */
    private void redrawBufferOnRejectedBidEvent(Event event) {
        bufferTableModel.removeRow(event.getBufferIndex());

        for (int i = 0; i < bufferTableModel.getRowCount(); i++) {
            bufferTableModel.setValueAt("", i, BUFFER_TABLE_COLUMN_NAMES.size() - 1);
        }

        bufferTableModel.insertRow(event.getBufferIndex(), List.of(event.getBufferIndex() + 1,
            BufferCellStatus.FREE.getDescription(), "", "<=====").toArray());
    }

    /**
     * Перерисовка таблицы приборов в случае, если заявка поступила на прибор
     *
     * @param event - заявка, поступившая на прибор
     */
    private void redrawDevicesOnDeviceBidEvent(Event event) {
        devicesTableModel.setValueAt(event.getBid().getName(), event.getDeviceNumber() - 1, 1);
        devicesTableModel.setValueAt(TIME_FORMAT.format(event.getEndProcessingTime()), event.getDeviceNumber() - 1, 2);
    }

    /**
     * Перерисовка таблицы приборов в случае, если заявка исполнена прибором.
     *
     * @param event - исполненная заявка
     */
    private void redrawDevicesOnCompletedBidEvent(Event event) {
        devicesTableModel.setValueAt("", event.getDeviceNumber() - 1, 1);
        devicesTableModel.setValueAt("", event.getDeviceNumber() - 1, 2);
    }

    /**
     * Перерисовка таблицы источников в случае, если заявка сгенерирована источником.
     *
     * @param event - сгенерированная заявка
     */
    private void redrawProducersTableOnGeneratedBidEvent(Event event) {
        producersTableModel.setValueAt(event.getBid().getName(),
            event.getBid().getProducer().getId() - 1, 1);
        producersTableModel.setValueAt(TIME_FORMAT.format(event.getTime()),
            event.getBid().getProducer().getId() - 1, 2);
    }

    /**
     * Метод выполнения автоматического режима.
     *
     * @param actionEvent - событие
     */
    @SuppressWarnings("StatementWithEmptyBody")
    private void autoModeButtonActionPerformed(ActionEvent actionEvent) {
        while (nextStepButtonActionPerformed(actionEvent)) {
        }
    }

    private void showStatisticsButtonActionPerformed(ActionEvent actionEvent) {
        dispose();
        new TotalStatisticsFrame(massServiceSystemController.getServiceSystemParams());
    }
}
